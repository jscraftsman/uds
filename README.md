# Setup
1. Extract "UdemyDownloader.zip"

2. **[SERVER]** Download NodeJS
    - https://nodejs.org/en/

3. **[SERVER]** Start server using "npm start"
    - CMD at root dir of "UdemyDownloader/"
    - CTRL+C to stop server

4. **[CLIENT]** Install CJS (Custom Javascript for Websites) chrome plugin
   - https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija
   - A "cjs" icon should appear beside the address bar (right side of the star)


5. **[CLIENT]** Paste content of "frontend.js" to CJS
    - Go to Udemy first
    - Enable "enable cjs for this host"
    - Load jQuery 1.11.0
    - Click "Save"

6. **[CLIENT]** Go to dashboard (first or target episode)

7. **[CLIENT]** Click "Start Download"
    - Videos will be downloaded in the videos/ folder.
    - Make sure videos/ folder is empty before starting the downloader (unless you want to mix the videos from different courses)
    - Press F12 on chrome to open th console
    - "Start Download" should turn into "Stop Download"

# Notes
- Do not close the Udemy page while downloading
- Do not close the CMD (server) while downloading
- Monitor the udemy page and cmd from time to time to check if the downloader is still running (check the console logs of Chrome)
- After completing a "course" move the files to a different folder before downloading a different course

# Commands
- Start Server - npm start
- Stop Server - CTRL + C

